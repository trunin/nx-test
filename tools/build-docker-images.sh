# #!/bin/sh

CI_REGISTRY_IMAGE=$1
BUILD=$2
VERSION=`jq --arg BUILD $BUILD --raw-output '.version + "-next" + $BUILD' package.json`
APPS=`ls dist/apps`

for APP in $APPS
do
    echo "Building $APP:$VERSION"
    docker build -f apps/$APP/Dockerfile \
      -t $CI_REGISTRY_IMAGE/$APP:$VERSION \
      -t $CI_REGISTRY_IMAGE/$APP:latest . || exit 1
done

for APP in $APPS
do
    echo "Pushing $APP:$VERSION"
    docker push $CI_REGISTRY_IMAGE/$APP:$VERSION || exit 1
    docker push $CI_REGISTRY_IMAGE/$APP:latest || exit 1
done
