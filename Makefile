install:
	docker-compose -f docker-compose.builder.yml run --rm install
affected-test:
	docker-compose -f docker-compose.builder.yml run --rm affected-test
affected-lint:
	docker-compose -f docker-compose.builder.yml run --rm affected-lint
lint:
	docker-compose -f docker-compose.builder.yml run --rm lint
build:
	docker-compose -f docker-compose.builder.yml run --rm build
dev:
	docker-compose up
bash:
	docker-compose -f docker-compose.builder.yml run --rm base bash
