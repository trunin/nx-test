# Docker development

```bash

# Install
$ make install

# Affected test
$ make affected-test

# Affected lint
$ make affected-lint

# Build & Run
$ make dev
```

# Without docker:

#
```bash

# Install
npm ci

# Affected test
$ npm run affected:test

# Affected lint
$ npm run affected:lint

# Build & Run
$ npm run foo:start
```
